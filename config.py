# config.py
from os.path import abspath, dirname, join

_cwd = dirname(abspath(__file__))

SECRET_KEY = 'flask-session-insecure-secret-key'
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + join(_cwd, 'inventory.db')
SQLALCHEMY_ECHO = True
UPLOAD_FOLDER = 'uploads/product_images'