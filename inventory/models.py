from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

IN = 0
OUT = 1


class Product(db.Model):
    __tablename__ = "products"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    number = db.Column(db.String)
    price = db.Column(db.String)
    image = db.Column(db.String)
    total_qty = db.Column(db.Integer)

    tags = db.relationship('Tag', backref='product')

    transactions = db.relationship('Transaction', backref='product')

    def qty(self):
        total_quantity = 0
        for transaction in self.transactions:
            if transaction.direction == IN:
                total_quantity = total_quantity + transaction.amount
            elif transaction.direction == OUT:
                total_quantity = total_quantity - transaction.amount
        return total_quantity

    # def toJSON(self):
    #     return {
    #         'id': id,
    #         'name': name,
    #         'number': number,
    #         'price': price,
    #         'tags': [tag.toJSON() for tag in tags],
    #         'qty': self.qty()
    #     }


class Tag(db.Model):
    __tablename__ = "tags"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))


class Transaction(db.Model):
    __tablename__ = "transactions"

    id = db.Column(db.Integer, primary_key=True)
    direction = db.Column(db.Integer)
    amount = db.Column(db.Float)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
