from flask import (
    Blueprint,
    flash,
    Markup,
    redirect,
    render_template,
    url_for,
    request,
    jsonify,
    request,
    current_app,
    send_file)
from werkzeug.utils import secure_filename
import os
import datetime

from .models import *

inventory = Blueprint("inventory", __name__)


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@inventory.route("/")
def index():
    products = Product.query.all()
    return render_template("index.jinja", products=products)


@inventory.route("/product/add", methods=['GET'])
def product_add():
    return render_template("edit_product.jinja", product={})


@inventory.route('/product/<int:product_id>')
def product_page(product_id=None):
    return render_template("product.jinja",
                           product=Product.query.get(product_id))


@inventory.route('/product/<int:product_id>/edit', methods=['GET'])
def product_edit(product_id=None):
    saved = request.args.get('saved', False)
    return render_template("edit_product.jinja",
                           product=Product.query.get(product_id),
                           saved=saved)


@inventory.route('/product', methods=['POST'])
@inventory.route('/product/<int:product_id>', methods=['POST'])
def product_edit_submit(product_id=None):
    product = Product.query.filter_by(id=product_id).first()
    if not product:
        product = Product()
        db.session.add(product)

    product.name = request.form['product_name']
    product.number = request.form['product_number']
    product.price = request.form['price']

    db.session.commit()

    tag_list = request.form.getlist('tag')

    for tag in Tag.query.filter_by(product_id=product.id).all():
        if tag.name not in tag_list:
            db.session.delete(tag)
        else:
            tag_list.remove(tag.name)

    db.session.commit()

    for tag in tag_list:
        if not Tag.query.filter(Tag.name==tag, Tag.product_id==product.id).first():
            tag = Tag(name=tag, product_id=product.id)
            db.session.add(tag)
            db.session.commit()

    image = request.files['image']
    if image and allowed_file(image.filename):
        filename = secure_filename(image.filename)
        image_filename = "{}_{}".format(datetime.datetime.now().isoformat(),
                                        filename)
        image_path = "{}/{}/{}".format(os.path.dirname(__file__),
                                          current_app.config['UPLOAD_FOLDER'],
                                          image_filename)

        image.save(image_path)

        product.image = image_filename
        db.session.commit()

        # return redirect(url_for('.uploaded_file', filename=filename))
    
    return redirect("/product/{}?saved=true".format(product.id))

@inventory.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_file(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))


@inventory.route('/product/<int:product_id>/change_qty', methods=["POST"])
def change_qty(product_id):
    product = Product.query.get(product_id)
    if not product.total_qty:
        product.total_qty = 0
    if request.form['direction'] == IN:
        product.total_qty = product.total_qty = product.total_qty + request.form['qty']
        transaction = Transaction(direction=IN,
                                  amount=request.form['qty'],
                                  product=product)
    elif request.form['direction'] == OUT:
        product.total_qty = product.total_qty = product.total_qty - request.form['qty']
        transaction = Transaction(direction=OUT,
                                  amount=request.form['qty'],
                                  product=product)
    db.session.commit()

