$(function(){

    $("#searchinput").on("keyup", function(event){
        search = $(event.target).val().toLowerCase();
        $(".product").each(function(){
            tags = $(this).data("tags").toLowerCase();
            number = $(this).data("number").toLowerCase();
            name = $(this).data("name").toLowerCase();
            if( search === '' || (tags.indexOf(search) >= 0 
                                  || number.indexOf(search) >= 0 
                                  || name.indexOf(search) >= 0) ){
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });
    
    $("#searchclear").on('click', function(event){
        event.preventDefault();
        console.log('clear search');
        $("#searchinput").val("").keyup();
    })

});